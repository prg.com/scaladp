object Light {
  def on() {
    println("The light is on.")
  }
  def off() {
    println("The light is off.")
  }
}

object Switch {
  val commands = collection.mutable.Buffer[() => Unit]()
  def store(cmd: () => Unit) { commands.append(cmd) }
  def execute() {
    commands.foreach(_.apply())
  }
}


//val turnOn = { () => Light.on()}
//val turnOff = { () => Light.off()}
Switch.store(Light.on)
Switch.store(Light.off)
Switch.execute()


