package factory_method.java;

public interface IFruitFactory {
	public IFruit createFruit(boolean mature);
}
