package proxy.java;

interface IImage {
    public void displayImage();
}

class RealImage implements IImage {

    private String filename = null;
    public RealImage(final String FILENAME) {
        filename = FILENAME;
        loadImageFromDisk();
    }

    private void loadImageFromDisk() {
        System.out.println("Loading   " + filename);
    }

    public void displayImage() {
        System.out.println("Displaying " + filename);
    }

}

class ProxyImage implements IImage {

    private RealImage image = null;
    private String filename = null;
    public ProxyImage(final String FILENAME) {
        filename = FILENAME;
    }

    public void displayImage() {
        if (image == null) {
            image = new RealImage(filename);
        }
        image.displayImage();
    }
}

public class VirtualProxy {
    public static void main(String[] args) {
        final IImage IMAGE1 = new ProxyImage("HiRes_10MB_Photo1");
        final IImage IMAGE2 = new ProxyImage("HiRes_10MB_Photo2");

        IMAGE1.displayImage(); // loading necessary
        IMAGE1.displayImage(); // loading unnecessary
        IMAGE2.displayImage(); // loading necessary
        IMAGE2.displayImage(); // loading unnecessary
        IMAGE1.displayImage(); // loading unnecessary
    }

}
