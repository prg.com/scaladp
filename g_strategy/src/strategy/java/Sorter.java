package strategy.java;

public interface Sorter {
    public int[] sort(int[] src);
}

class BubbleSorter implements Sorter {
    @Override
    public int[] sort(int[] src) {
        // fake bubble sort
        return src.clone();
    }
}

class QuickSorter implements Sorter {
    @Override
    public int[] sort(int[] src) {
        // fake quick sort
        return src.clone();
    }
}

class HeapSorter implements Sorter {
    @Override
    public int[] sort(int[] src) {
        // fake heap sort
        return src.clone();
    }
}

class Client {
    public static void main(String[] args) {
        final int[] data = new int[100];
        for (int i = 0; i < 100; i++)
            data[i] = (int) (Math.random() * 100);
        Sorter[] sorters = new Sorter[] {
            new BubbleSorter(), new QuickSorter(), new HeapSorter()
        };
        for (final Sorter sorter : sorters)
            new Thread(new Runnable() {
                @Override
                public void run() {
                    sorter.sort(data.clone());
                }
            }).start();
    }
}